import config
from neuthink.graph import basics as bg
from neuthink.graph import neo4j_graph as pg
from typing import List

graph = pg.PersistentGraph(config.graph_connection, graph_segment="segment_graph")      #подключаемся к графу

#[Пытаемся открыть файл и считать данные из файла]
try:
    file = open('words.txt','r')
except IOError:
    print("Can't read file")
graph_words = file.readlines()

def make_graph(words:List[str])->List[str]:
    """[Данная функция получает массив строк, сплитует строки по пробелам на массивы массивов, а затем создает узлы графа, в которые кладется значения из массивов]
    
    Arguments:
        words {[List[str]]} -- [список строк, считанных из файла]
    
    Returns:
        [List[Node]] -- [список узлов графа]
    """
    if graph.Match({})==[]: #если граф пустой
        splited_words = [i.split() for i in words] #разделяем строку на список слов 
        return [bg.Node(graph,{i[0]:i[1],i[2]:i[3],i[4]:i[5]}) for i in splited_words] #создаем узлы
        
if __name__=='__main__':
    make_graph(graph_words)