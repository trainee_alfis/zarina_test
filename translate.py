import config
from neuthink.graph import basics as bg
from neuthink.graph import neo4j_graph as pg

graph = pg.PersistentGraph(config.graph_connection, graph_segment="segment_graph")      #подключаемся к графу

def deleteSymbols(sentence:str)->str:
    """[Функция, которая удаляет символы в предложении]
    
    Arguments:
        sentence {[str]} -- [предложение, введенное пользователем]
    
    Returns:
        [str] -- [измененное предложение]
    """
    return "".join([letter.lower() for letter in sentence if letter.isalpha() or letter == " "])

def translator():   
    """[Функция, где реализуется ввод пользователем предложения, а затем оно переводится на англ. язык]
    """
    sentence = input("введите ваше предложение, которое вы хотите перевести: \n")
    language = input("На каком языке было введено предложение?").lower()
    rlanguage = "русский" if language == "английский" else "английский"
    result = []
    sentence = deleteSymbols(sentence).split(" ")
    for word in sentence:
        tr_word = graph.Match({language: word})  #поиск узла графа по языку, на котором пользователь ввел сообщение и словам из предложение
        result += [tr_word[0][rlanguage]] if len(tr_word) > 0 else [word] #добавление найденных слов в массив строк
    print("Ваш перевод:\n" + " ".join(result)) #соединяем предложение


if __name__=='__main__':
    translator()
